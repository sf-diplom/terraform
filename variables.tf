variable "cloud_id" {
  type    = string
  default = "b1gao98a7hlpttth939v"
}

variable "folder_id" {
  type    = string
  default = "b1gpnhho5vqgdin7nli5"
}

variable "zone" {
  type    = string
  default = "ru-central1-a"
}
