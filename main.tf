terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file("./key.json")
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.zone
}

module "network" {
  source       = "./modules/network"
  network_name = "sf-network"
  subnet_name  = "sf-subnet"
  zone         = "ru-central1-b"
  cidr_block   = "10.0.0.0/24"
}

module "load_balancer" {
  source            = "./modules/load_balancer"
  target_group_name = "sf-target-group"
  region_id         = "ru-central1"
  targets = [for ip in module.instances.worker_internal_ips : {
    subnet_id = module.network.subnet_id
    address   = ip
  }]
  load_balancer_name = "sf-load-balancer"
  listener_port      = 8080
  health_check_port  = 3003
  health_check_path  = "/ping"
  depends_on         = [module.instances, module.network]
}

module "instances" {
  source                    = "./modules/instances"
  subnet_id                 = module.network.subnet_id
  zone                      = "ru-central1-b"
  image_family              = "ubuntu-2004-lts"
  core_fraction             = 20
  allow_stopping_for_update = true
  scheduling_policy         = true
  worker_instance_count     = 1
  cp_instance_count         = 1
  srv_instance_count        = 1
  worker_instance_name      = "app"
  cp_instance_name          = "master"
  srv_instance_name         = "srv"
  worker_cores              = 2
  cp_cores                  = 2
  srv_cores                 = 2
  worker_memory             = 2
  cp_memory                 = 2
  srv_memory                = 4
  disk-size                 = 10
  worker_cloud-init_path    = "init-files/cloud-init-worker.yaml"
  cp_cloud-init_path        = "init-files/cloud-init-cp.yaml"
  srv_cloud-init_path       = "init-files/cloud-init-srv.yaml"
}

module "dns" {
  source               = "./modules/dns"
  private_networks     = [module.network.network_id]
  instance_ips         = concat(module.instances.worker_internal_ips, module.instances.cp_internal_ips, module.instances.srv_internal_ips)
  instance_names       = concat(module.instances.worker_external_names, module.instances.cp_external_names, module.instances.srv_external_names)
  dns_name             = "skillfactory-diplom.ru."
  dns_zone_name        = "skillfactory-diplom-ru"
  srv_fqdn             = module.instances.srv_external_ips
  dns_zone_description = "skillfactory-diplom-ru"
  depends_on           = [module.instances, module.network]
}
