# Описание инфраструктуры выполнено с помощью Terraform


## Описание

В дипломном проекте были использованы модули Terraform. В файл main.tf вводятся значения для описания следующих элементов: 
- network
- load_balancer
- instances
- dns

## Применение модулей

- склонировать репозиторий командой git clone https://gitlab.com/sf-diplom/terraform.gitlab
- Перейти в локальную папку с репозиторием
- Выполнить последовательно terraform init, terraform plan, terraform apply.

## Изменение параметров
Если необходимо изменить параметры для развертывания инфраструктуры, то необходимо внести новые значения в файл main.tf, который находится в корне репозитория. Описания переменных находятся в файле variables.tf в директории каждого модуля.

## Переменные значений используемые в модулях.

### Модуль network source      
**network_name** - Имя создаваемой сети в Yandex Cloud \
**subnet_name** - Имя создаваемой подсети в VPC \
**zone** - Зона доступности для создаваемой подсети    
**cidr_block** - CIDR блок для подсети, определяющий диапазон доступных IP-адресов  

### Модуль load_balancer      
 **target_group_name** - Имя группы целевых объектов \
 **region_id** - Идентификатор региона, в котором создается группа целевых объектов \
 **targets** - Список целевых инстансов для группы целевых объектов     
**load_balancer_name** - Имя сетевого балансировщика нагрузки \
**listener_port** - Порт, на котором слушает балансировщик нагрузки \
**health_check_port** - Порт, используемый для проверки работоспособности \
**health_check_path** - Путь, используемый для проверки работоспособности 

### Модуль instances
 **subnet_id** - ID подсети \
 **zone** - Зона доступности для инстанса \
 **image_family** - Семейство образа для инстанса  
 **core_fraction** - Доля CPU \
 **allow_stopping_for_update** - Разрешение остановки ВМ для обновления конфигурации  
**scheduling_policy** - Сделать ВМ прерываемой \
**worker_instance_count** - Количество создаваемых инстансов worker\
 **cp_instance_count** - Количество создаваемых инстансов cp \
 **srv_instance_count** - Количество создаваемых инстансов srv \
 **worker_instance_name** - Имя инстанса worker \
 **cp_instance_name** - Имя инстанса cp \
 **srv_instance_name** - Имя инстанса srv \
 **worker_cores** - Количество CPU ядер для инстанса worker \
 **cp_cores** - Количество CPU ядер для инстанса cp \
 **srv_cores** - Количество CPU ядер для инстанса srv \
 **worker_memory** - Количество памяти для инстанса worker   
 **cp_memory** - Количество памяти для инстанса cp \
 **srv_memory** - Количество памяти для инстанса srv \
 **disk-size** - Размер диска \
 **worker_cloud-init_path** - Путь к файлу cloud-init для worker \
 **cp_cloud-init_path** - Путь к файлу cloud-init для cp \
 **srv_cloud-init_path** - Путь к файлу cloud-init для srv 

### Модуль DNS
**private_networks** - Список ID приватных сетей для доступа к DNS зоне      
**instance_ips** - Список IP-адресов инстансов для A записей           
**instance_names** - Список имен инстансов для создания A записей         
**dns_name** - Доменное имя для DNS зоны               
**dns_zone_name** - Имя DNS зоны в Yandex Cloud          
**srv_fqdn** - Описание DNS зоны               
**dns_zone_description** - Описание DNS зоны   
   



  

